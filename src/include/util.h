#ifndef _UTIL_H
#define _UTIL_H

//Common includes
#include "config.h"
#include "fe310g002.h"

//Bit manipulation
#define _BV(bit) (1<<(bit))
#define bit_is_set(val, bit) (val & _BV(bit))
#define bit_is_clear(val, bit) (!(val & _BV(bit)))
#define loop_until_bit_is_set(val, bit) do{} while(bit_is_clear(val, bit))
#define loop_until_bit_is_clear(val, bit) do{} while(bit_is_set(val, bit))

//Rounded integer division without floats
#define DIV_ROUND_CLOSEST(n, d) ((((n) < 0) ^ ((d) < 0)) ? (((n) - (d)/2)/(d)) : (((n) + (d)/2)/(d)))

#endif
