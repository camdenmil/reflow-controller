#ifndef _UART_H
#define _UART_H

#include "fe310g002.h"
#include <stdint.h>

void uart_init(volatile UART_t*, uint32_t);
void send_byte(volatile UART_t*, uint8_t);

#endif
