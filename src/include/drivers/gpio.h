#ifndef _GPIO_H
#define _GPIO_H

#include <stdint.h>
#include "util.h"

void map_iof(uint8_t, uint8_t);

#endif
