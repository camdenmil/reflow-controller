#include <stdint.h>
#include "util.h"
#include "drivers/uart.h"

uint8_t message[] = "This is a bit RISCy. Here's some more text to send so I can see what's up.";

int main(void){
    //Configure GPIO
    GPIO_output_en = (1<<0);

    //Turn the LCD backlight on
    GPIO_output_val = (1<<0);

    uart_init(UART0, 9600);

    for(int i=0; message[i] != 0; i++){
        send_byte(UART0, message[i]);
    }

    while(1){
        send_byte(UART0, (uint8_t)0x55);
    }
    return 0;
}

