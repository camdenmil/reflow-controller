#include <stdint.h>
#include "util.h"

//linker-generated symbols
extern uint32_t _stext;
extern uint32_t _etext;
extern uint32_t _sbss;
extern uint32_t _ebss;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sstack;
extern uint32_t _estack;

int main(void);

void wait_ms(int ms){
    int j = ms * 8000; //2 instructions per loop iteration, assume 14Mhz
    for (int i = 0; i<j; i++){
        asm(""); //Take the CPU for a spin
    }
}

// Set the CPU to 320Mhz
void ludicrous_speed(){
    uint32_t temp = 0;
    temp |= _BV(PRCI_pllcfg_pllrefsel); //select 16Mhz crystal
    temp |= (1 << PRCI_pllcfg_pllr); //PLLR=2 for 8Mhz
    temp |= (((80/2) -1 ) << PRCI_pllcfg_pllf); //VCO * 40 for 640Mhz out
    temp |= _BV(PRCI_pllcfg_pllq); //PLLQ=2 for 320Mhz
    PRCI_pllcfg = temp;

    wait_ms(1000);
    while( (PRCI_pllcfg & (1 << PRCI_pllcfg_plllock)) == 0); //Wait till the PLL locks
    PRCI_pllcfg |= (1 << PRCI_pllcfg_pllsel); //select the PLL for hfclk
}

// Set the CPU to 16Mhz
void slow_speed(){
    PRCI_pllcfg = 0x00070DF1; //Use 16Mhz crystal for hfclk
}

void reset_handler(void){

    //Initialize data by copying variables from .text to ram
    uint32_t *psrc = &_etext;
    uint32_t *pdest = &_sdata;

    //If there are any variables to copy...
    //uint32_t is type-agnostic here since .data is word-aligned
    if (psrc != pdest) {
        for(; pdest < &_edata;) {
            *pdest++ = *psrc++;
        }
    }

    //Clear out .bss
    for (pdest = &_sbss; pdest < &_ebss;) {
        //Haven't init'd libc yet so shouldn't try using memset
        *pdest++ = 0;
    }

    ludicrous_speed();

    //Jump to main
    main();

    //Spin the CPU since a this function could have been called
    //by a reset or from _start (i.e. the stack could be BS)
    while(1);
}

