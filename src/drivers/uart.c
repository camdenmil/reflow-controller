#include "util.h"
#include "drivers/gpio.h"
#include "drivers/uart.h"

void uart_init(volatile UART_t* uart, uint32_t baud){
    //Setup the baud rate divisor
    uint32_t div = DIV_ROUND_CLOSEST(HFCLK,baud) - 1;
    uart->div.bit.div = div;

    //Enable full duplex
    uart->txctrl.bit.txen = 1;
    uart->rxctrl.bit.rxen = 1;

    //Enable in GPIO
    //TODO fix this hard-coding
    map_iof(16, 0);
    map_iof(17, 0);
}

void send_byte(volatile UART_t* uart, uint8_t byte){
    //Wait until the FIFO is empty
    while(uart->txdata.bit.full);

    uart->txdata.bit.data = byte;
}
