#include "drivers/gpio.h"

//TODO make a proper way for mapping IOF without magic numbers
void map_iof(uint8_t pin, uint8_t iof){
    //Select the desired IO map
    GPIO->iof_sel |= ((iof & 1) << pin);
    //Enable it
    GPIO->iof_en |= _BV(pin);
}
