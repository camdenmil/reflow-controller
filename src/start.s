.global _start
_start:
    # Move traps to zero which is a safe landing spot
    csrrw x0, mtvec, x0
    # disable interrupts
    addi x5, x0, 0x8
    csrrc x5, mstatus, x0
    fence rw, rw
    # Initialize the stack
    la x2, _estack
    # Jump to the reset handler which will start main
    j reset_handler
    # the app returned, wait for debugger
    sbreak
    j .

# Empty interrupt handler
.global dummy_handler
dummy_handler:
    jalr x0, 0(x1)
