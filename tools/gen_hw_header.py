#!/usr/bin/env python

import argparse
import os
import sys
import yaml

def helper_defines():
    ret = []
    ret.append('#include <stdint.h>')
    ret.append('#define _ABS_ADDR(mem_addr) (*(volatile uint32_t *)(mem_addr))')
    return ret

def device_defs(name, device):
    registers = {k: v for k, v in sorted(device.items(), key=lambda reg: reg[1]['offset'])}
    output = []

    output.append('')
    output.append('// Device definitions for %s'%name)
    output.append('')

    for reg, attr in registers.items():
        if 'fields' in attr:
            output.append('')
            output.append('// Definitions for register %s'%reg)
            output.append('')
            output.extend(reg_union('%s_%s'%(name, reg), attr))
            output.append('')
            output.append('// Field-level defines')
            output.extend(reg_defines('%s_%s'%(name, reg), attr))

    output.append('')
    output.append('// Device struct')
    output.extend(device_struct(name, registers))
    return output

def get_reg_type(name, reg, device):
    if 'size' in reg:
        if reg['size'] in [4,8]:
            return 'uint%d_t %s'%(reg['size']*8, name)
        else:
            return 'uint32_t %s[%d]'%(name, reg['size'])
    elif 'fields' not in reg:
        return 'uint32_t %s'%name
    else:
        return '%s_%s_t %s'%(device, name, name)

def device_struct(name, registers):
    output = []
    output.append('typedef struct {')

    last_offset = next(iter(registers.values()))['offset']
    last_size = 4
    for reg, attr in registers.items():
        offset = attr['offset']
        if offset < last_offset:
            continue
        if offset > last_offset+last_size:
            output.append('    {:<28} // pad 0x{:X} to 0x{:X}'.format(
                'uint32_t padding_%X[%d];'%(offset, (offset-(last_offset+last_size))/4),
                last_offset+last_size,
                offset-1)
            )
        last_offset = offset
        last_size = 4 if 'size' not in attr else attr['size']
        output.append('    {0:<28} // offset: 0x{1:X}'.format(
            get_reg_type(reg, attr, name) + ';',
            attr['offset'])
        )
    output.append('} %s_t;'%name)
    offsets = [x['offset'] for x in registers.values()]
    last = list(registers.items())[-1][1]
    size = (last['offset'] + (4 if 'size' not in last else last['size'])) - list(registers.items())[0][1]['offset'] 
    output.append('_Static_assert(sizeof({0}_t) == {1:d}, \"Size of {0} must be {1:d} bytes\");'.format(
        name,
        size)
    )
    return output

def reg_union(name, register):
    fields = {k: v for k, v in sorted(
            register['fields'].items(),
            key=lambda field: field[1] if isinstance(field[1], int) else field[1]['low']
        )
    }
    output = []
    output.append('typedef union {')
    output.append('    struct{')

    def bitnum(first, last):
        if first != last:
            return '{:>2}..{:<2}'.format(first, last)
        else:
            return '{:>6}'.format(first)

    last_bit = -1
    for field, attr in fields.items():
        length = 1 if isinstance(attr, int) else attr['high'] - attr['low'] + 1
        bit = attr if isinstance(attr, int) else attr['low']
        high = attr if isinstance(attr, int) else attr['high']
        if bit <= last_bit:
            continue
        if bit > last_bit + 1:
            output.append('{0:}{1:<32} //bit: {2:<6} reserved'.format(
                ' '*8,
                'uint32_t :%d;'%(bit - last_bit - 1),
                bitnum(last_bit+1, bit -1))
            )
        last_bit = high
        output.append('{0:}{1:<32} //bit: {2:<6} {3:}'.format(
            ' '*8,
            'uint32_t %s:%d;'%(field, length),
            bitnum(bit, high),
            field)
        )
    if last_bit != 31:
        output.append('{0:}{1:<32} //bit: {2:<6} reserved'.format(
            ' '*8,
            'uint32_t :%d;'%(31 - last_bit),
            bitnum(last_bit+1, 31))
        )
    output.append('    } bit;')
    output.append('    uint32_t reg;')
    output.append('} %s_t;'%name)
    return output

def reg_defines(name, register):
    output = []
    mask_all = 0
    for field, attr in register['fields'].items():
        length = 1 if isinstance(attr, int) else attr['high'] - attr['low'] + 1
        bit = attr if isinstance(attr, int) else attr['low']
        mask = (2**length)-1
        mask_all = mask_all | (mask << bit)
        posname = '%s_%s'%(name, field)
        output.append('#define {0:<40} {1:}'.format(
            posname,
            bit)
        )
        if length > 1:
            output.append('#define {0:<40} {1:}'.format(
                '%s_%s_mask'%(name, field),
                '((uint32_t)(0x{0:X} << {1:}))'.format(mask, posname))
            )
    output.append('#define {0:<40} ((uint32_t)0x{1:X})'.format(
        '%s_mask'%name,
        mask_all)
    )
    return output

def instantiate_device(name, typ, address, device):
    output = []
    output.append('#define {0:<18} ((volatile {1:<14}*)0x{2:08X})'.format(
        name, typ, address)
    )
    for reg, attr in device.items():
        output.append('#define {:<40} _ABS_ADDR(0x{:X})'.format(
            '%s_%s'%(name, reg),
            address + attr['offset'])
        )
    return output

def genPLIC(data):
    data.pop('_gen_function')
    plic = {}
    interrupts = ['ZERO_INTERRUPT']
    for dev, interrupt in data.items():
        prio_field = {
            'priority':{
                'low': 0,
                'high':2
        }}
        if isinstance(interrupt, int):
            name = str(dev)
            plic['prio_'+name] = {
                'offset': interrupt * 4,
                'fields': prio_field
            }
            interrupts.append(name)
        else:
            for i in range(interrupt['start'], interrupt['end']+1):
                name ='%s_%s'%(dev, i-interrupt['start'])
                plic['prio_'+name] = {
                    'offset': i * 4,
                    'fields': prio_field
                }
                interrupts.append(name)
    plic['pending1'] = {
        'offset': 0x1000,
        'fields': {interrupts[i]+'_pending': i for i in range(0,32)}}
    plic['pending2'] = {
        'offset': 0x1004,
        'fields': {interrupts[i]+'_pending': i-32 for i in range(32,53)}}
    plic['enable1'] = {
        'offset': 0x2000,
        'fields': {interrupts[i]+'_enable': i for i in range(0,32)}}
    plic['enable2'] = {
        'offset': 0x2004,
        'fields': {interrupts[i]+'_enable': i-32 for i in range(32,53)}}
    plic['threshold'] = {
        'offset': 0x20000,
        'fields': {
            'threshold': {
                'low': 0,
                'high': 2
            }
        }
    }
    return plic

def genAON(data):
    data.pop('_gen_function')
    aon = data['registers']
    for i in range(data['backup']['num_backups']):
        aon['backup_%d'%i] = {
            'offset': data['backup']['offset'] + (i*4),
            'size': 4
        }
    return aon

def genPMU(data):
    data.pop('_gen_function')
    pmu = data['registers']
    for i in range(data['sleep_program']['length']):
        pmu['pmusleepi%d'%i] = {
            'offset': data['sleep_program']['offset'] + (i*4),
            'size': 4
        }
    for i in range(data['wake_program']['length']):
        pmu['pmuwakeupi%d'%i] = {
            'offset': data['wake_program']['offset'] + (i*4),
            'size': 4
        }
    return pmu

def yaml2header(indir, out):
    if not os.path.isdir(indir):
        sys.exit('Could not find %s'%indir)
    files = os.listdir(indir)
    data = {}
    for f in [g for g in files if os.path.splitext(g)[1] == '.yaml']:
        with open(f) as fin:
            fdata = yaml.load(fin, yaml.FullLoader)
        for d in fdata:
            if d in data:
                data[d].update(fdata[d])
            else:
                data[d] = fdata[d]

    for device, defs in data['autodevices'].items():
        print('Auto-generating definition for %s.'%device)
        data['devices'][device] = globals()[defs['_gen_function']](defs)

    guard = data['device_name'].upper().replace('-','')
    out.write('#ifndef %s_H\n'%guard)
    out.write('#define %s_H\n'%guard)
    out.write('\n'.join(helper_defines()))
    out.write('\n')

    out.write('\n// Device definitions\n')
    for name, device in data['devices'].items():
        out.write('\n'.join(device_defs(name, device)))
        out.write('\n')

    out.write('\n// Memory Map\n\n')
    for device, attrs in data['memory_map'].items():
        if 'type' not in attrs:
            out.write('#define {:<18} _ABS_ADDR(0x{:X})\n'.format(
                device,
                attrs['base'])
            )
            out.write('#define {:<18} 0x{:X}\n'.format(
                device+'_length',
                attrs['top'] - attrs['base'])
            )
        else:
            out.write('\n'.join(instantiate_device(
                device,
                attrs['type']+'_t',
                attrs['base'],
                data['devices'][attrs['type']])
            ))
            out.write('\n')
    out.write('\n')

    out.write('#endif\n')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'indir',
        help='path to directory containing hardware def yaml files',
        type=str)
    parser.add_argument(
        '-o',
        dest='outfile',
        help='File to write C header output to',
        type=str)

    args = parser.parse_args()

    if args.outfile is None:
        out = sys.stdout
    else:
        out = open(args.outfile, 'w')

    yaml2header(args.indir, out)

