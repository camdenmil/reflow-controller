# reflow-controller

This contains two executable projects. `bootloader` is a self-contained program intended to be flashed at the base of external flash. See the README in that directory for more information on the bootloader's functionality. The rest of this repo contains the application code that nominally lives in the rest of external flash.

