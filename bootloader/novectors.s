.section .text.entry, "ax"
.global _start
_start:
    # Move traps to zero which is a safe landing spot
    csrrw x0, mtvec, x0
    # disable interrupts
    addi x5, x0, 0x8
    csrrc x5, mstatus, x0
    fence rw, rw
    # stack starts at the top of DTIM
    lui x2, 0x80004
    jal main
    # main is done, now jump to the app
    lui x5, 0x20010
    jalr x0, 0(x5)
    # the app returned, wait for debugger
    sbreak
    j .

.text

# Empty interrupt handler
.global dummy_handler
dummy_handler:
    jalr x0, 0(x1)
