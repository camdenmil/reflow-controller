# Bootloader

This is a basic bootloader that sits at the base of flash. This exists to allow for recovery of the system should application code in flash do something silly like disable all the oscillators. Like the Hifive Rev B, `0x20000000` - `0x2000FFFF` is reserved for the bootloader. If the boot process is not interupted, the bootloader will jump to `0x20010000`.

## Nominal Boot

Nominally, the bootloader configures GPIO11 (SERVO_PWM) as an input with the internal pullup enabled. After configuring GPIO input, GPIO0 (LED_PWM) will be strobed for 200ms. After 5 seconds, the bootloader will strobe GPIO0 for 200ms again. Afterwards, the GPIO peripheral is reset to its default POR state and the CPU is jumped to `0x20010000`.

## Interrupting Boot

The bootloader provides a window to halt the CPU should the code at `0x20010000` be incompatible with flashing new code. To interrupt boot, connect GPIO11 to to GND during boot or grab the CPU with a debugger during the 5 second window.
