#include "stdint.h"

#define IO(mem_addr) (*(volatile uint32_t *)(mem_addr))

//Define the hardware registers that we'll need
#define input_val  IO(0x10012000)
#define input_en   IO(0x10012004)
#define output_en  IO(0x10012008)
#define output_val IO(0x1001200C)
#define pue        IO(0x10012010)

//Board-specific values
#define BLINK_PIN 0
#define HALT_PIN  11

//Config
#define WAIT_TIME  5000
#define BLINK_TIME 200

void wait_ms(int ms){
    int j = ms * 7000; //2 instructions per loop iteration, assume 14Mhz
    for (int i = 0; i<j; i++){
        asm(""); //Take the CPU for a spin
    }
}
uint64_t read_cycles(void)
{
    uint64_t cycles;
    asm volatile ("rdcycle %0" : "=r" (cycles));
    return cycles;
}

int main (void) {
    //Configure GPIO
    uint32_t starting_input_en = input_en;
    input_en = (1<<HALT_PIN);
    pue = (1<<HALT_PIN);
    output_en = (1<<BLINK_PIN);

    //Blink the led
    output_val = (1<<BLINK_PIN);
    wait_ms(BLINK_TIME);
    output_val = 0;

    //Check the halt pin for a while
    uint64_t start_cycles = read_cycles();
    uint64_t end_cycles = start_cycles + (WAIT_TIME * 14000); //assume 14Mhz
    uint64_t current_cycles = start_cycles;
    while(current_cycles < end_cycles){
        current_cycles = read_cycles();
        if (current_cycles < start_cycles){
            // The CPU has been on for 41k years so the cycle counter overflowed...
            break;
        }
        if ((input_val & (1<<HALT_PIN)) == 0){
            while(1){
                asm("c.ebreak"); // Wait for the debugger
            }
        }
    }
    //Blink the led
    output_val = (1<<BLINK_PIN);
    wait_ms(BLINK_TIME);
    output_val = 0;

    //Cleanup
    output_en = 0;
    pue = 0;
    input_en = starting_input_en;
    return(0);
}
