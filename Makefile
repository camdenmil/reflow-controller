SRC_DIR = ./src

RISCVGNU = riscv64-unknown-elf

DEVICE_DEF := tools/fe310-g002.yaml

SRC := $(shell find $(SRC_DIR) -name '*.c' -or -name '*.s')
OBJ := $(addsuffix .o, $(basename $(SRC)))

INC_DIRS := $(shell find $(SRC_DIR) -type d)
INC_PARM = $(foreach d, $(INC_DIRS), -I$d)

AOPS = -march=rv32imac -mabi=ilp32
COPS = -march=rv32imac -mabi=ilp32 -Wall -O2 -nostdlib -nostartfiles -ffreestanding -c
LDOPS = -m elf32lriscv

all: controller

auto_header: $(DEVICE_DEF)
	tools/gen_hw_header.py tools/fe310-g002.yaml -o src/include/fe310g002.h

%.o: %.s
	$(RISCVGNU)-as $(AOPS) $< -o $@

%.o: %.c auto_header
	$(RISCVGNU)-gcc $(COPS) $< -o $@ $(INC_PARM)

controller : memmap $(OBJ)
	$(RISCVGNU)-ld $(LDOPS) $(OBJ) -T memmap -o controller.elf
	$(RISCVGNU)-objdump -D controller.elf -M no-aliases > controller.list

clean :
	find $(SRC_DIR) -type f -name '*.o' -delete
	rm -f src/include/fe310g002.h
	rm -f *.elf
	rm -f *.list
